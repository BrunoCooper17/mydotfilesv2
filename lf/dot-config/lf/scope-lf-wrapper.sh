#!/usr/bin/env bash

image() {
  file=$1
  w=$2
  h=$3
  x=$4
  y=$5

  if [[ "$(file -Lb --mime-type "$file")" =~ ^image ]]; then
    kitty +kitten icat --silent --stdin no --transfer-mode memory --place "${w}x${h}@${x}x${y}" "$file" < /dev/null > /dev/tty
    exit 1
  fi
}

[ ! -d "${XDG_CACHE_HOME:-$HOME/.cache}/lf" ] && mkdir "${XDG_CACHE_HOME:-$HOME/.cache}/lf"

case "$(file --dereference --brief --mime-type -- "$1")" in
  image/*)
    image "$1" "$2" "$3" "$4" "$5" "$1"
    ;;

  video/*)
    CACHE="${XDG_CACHE_HOME:-$HOME/.cache}/lf/thumb.$(stat --printf '%n\0%i\0%F\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | sha256sum | cut -d' ' -f1)"
    [ ! -f "$CACHE" ] && ffmpegthumbnailer -i "$1" -o "$CACHE" -s 0
    image "$CACHE" "$2" "$3" "$4" "$5" "$1"
    ;;

  */pdf)
    CACHE="${XDG_CACHE_HOME:-$HOME/.cache}/lf/thumb.$(stat --printf '%n\0%i\0%F\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | sha256sum | cut -d' ' -f1)"
    [ ! -f "$CACHE.jpg" ] && pdftoppm -jpeg -f 1 -singlefile "$1" "$CACHE"
    image "$CACHE.jpg" "$2" "$3" "$4" "$5" "$1"
    ;;

  */epub+zip | */mobi*)
    CACHE="${XDG_CACHE_HOME:-$HOME/.cache}/lf/thumb.$(stat --printf '%n\0%i\0%F\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | sha256sum | cut -d' ' -f1)"
    [ ! -f "$CACHE.jpg" ] && gnome-epub-thumbnailer "$1" "$CACHE.jpg"
    image "$CACHE.jpg" "$2" "$3" "$4" "$5" "$1"
    ;;

  *)
    "$HOME/.config/lf/ranger/scope.sh" "${1}" "${2}" "${3}" "" "" || true
    ;;
esac
exit 1
