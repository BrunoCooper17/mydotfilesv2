#!/usr/bin/env bash

kitty +kitten icat --clear --transfer-mode memory < /dev/null > /dev/tty
