vim.opt.smartcase = true
vim.opt.incsearch = true

vim.opt.cursorline = true
vim.opt.cursorlineopt = "line,number"

vim.opt.expandtab = false
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.smartindent = true

vim.opt.relativenumber = true

-- Highlight on_yank
local on_yank_autocmd = vim.api.nvim_create_autocmd
on_yank_autocmd("TextYankPost", {
	callback = function()
		vim.highlight.on_yank({
			higroup = "IncSearch",
			timeout = 750,
		})
	end,
})

-- Folding with tree-sitter
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

-- Unfold by default
local unfold_group = vim.api.nvim_create_augroup("unfold", { clear = true })
local unfold_autocmd = vim.api.nvim_create_autocmd
unfold_autocmd({ "BufReadPost", "FileReadPost" }, {
	pattern = "*",
	group = unfold_group,
	command = "normal zR",
})
