---@type MappingsTable
local M = {}

M.disabled = {
	n = {
		["s"] = "",

		-- Comment
		["<leader>/"] = "",

		-- Lsp rename
		["<leader>ra"] = "",

		-- Lsp formatting
		["<leader>fm"] = "",
		["<leader>f"] = "",

		-- Search and select?
		["gN"] = "",
		["gn"] = "",
	},
}

M.general = {
	n = {
		[";"] = { ":", "enter command mode", opts = { nowait = true } },
		-- ["<leader>ai"] = { "<cmd>ArduinoInfo<CR>", "Arduino Info" },

		-- Split buffer
		["<leader>tsk"] = { "<C-w>v", "Split Vertical" },
		["<leader>tsh"] = { "<C-w>s", "Split Horizontal" },
		["<leader>tsK"] = { "<C-w>t<C-w>H", "Change Split to Vertical" },
		["<leader>tsH"] = { "<C-w>t<C-w>K", "Change Split to Horizontal" },
		["<leader>tsx"] = { "<C-w>o", "Remove split" },

		-- Vertical navigation, but stay in the center... please
		["<C-b>"] = { "<C-b>zz" },
		["<C-f>"] = { "<C-f>zz" },
		["<C-u>"] = { "<C-u>zz" },
		["<C-d>"] = { "<C-d>zz" },

		-- Save
		["<leader>fs"] = { ":w", "Save File" },

		-- Code
		["<leader>c/"] = {
			function()
				require("Comment.api").toggle.linewise.current()
			end,
			"Toggle comment",
		},
		["<leader>cf"] = {
			function()
				vim.lsp.buf.format({ async = true })
			end,
			"LSP formatting",
		},

		-- diagnostics
		["gN"] = {
			function()
				vim.diagnostic.goto_prev({ float = { border = "rounded" } })
				vim.cmd.normal("zz")
			end,
			"Goto prev",
		},
		["gn"] = {
			function()
				vim.diagnostic.goto_next({ float = { border = "rounded" } })
				vim.cmd.normal("zz")
			end,
			"Goto next",
		},

		-- Refactoring
		["<leader>rn"] = {
			function()
				require("nvchad_ui.renamer").open()
			end,
			"LSP rename",
		},
	},
}

-- more keybinds!

return M
