-- To find any highlight groups: "<cmd> Telescope highlights"
-- Each highlight group can take a table with variables fg, bg, bold, italic, etc
-- base30 variable names can also be used as colors

local M = {}

---@type Base46HLGroupsList
M.override = {
	Comment = {
		italic = true,
		bold = true,
		fg = "light_grey",
	},

	CursorLine = {
		bg = "grey",
	},

	LineNr = {
		fg = "light_grey",
	},

	Search = {
		bg = "nord_blue",
		bold = true,
		italic = true,
	},

	IncSearch = {
		bg = "vibrant_green",
		bold = true,
		italic = true,
	},

	Visual = {
		bg = "grey",
		bold = true,
		italic = true,
	},

	Folded = {
		fg = "teal",
		bg = "grey",
		bold = true,
		italic = true,
	},

	FoldColumn = {
		fg = "teal",
		bg = "grey",
		bold = true,
		italic = true,
	},
}

---@type HLTable
M.add = {
	NvimTreeOpenedFolderName = {
		fg = "green",
		bold = true,
	},
}

return M
