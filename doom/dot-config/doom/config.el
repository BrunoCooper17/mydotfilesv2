;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "BrunoCooper17"
      user-mail-address "BrunoCooper17@outlook.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "JetBrainsMono Nerd Font" :size 18)
      doom-variable-pitch-font (font-spec :family "JetBrainsMono Nerd Font" :size 18)
      doom-serif-font (font-spec :family "JetBrainsMono Nerd Font" :size 18)
      doom-unicode-font (font-spec :family "JetBrainsMono Nerd Font" :size 18)
      doom-big-font (font-spec :family "JetBrainsMono Nerd Font" :size 18))
(after! doom-theme
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
;; (setq doom-theme 'doom-snazzy)
;; (setq doom-theme 'doom-spacegrey)
;; (setq doom-theme 'doom-city-lights)
;; (setq doom-theme 'doom-oceanic-next)
;; (setq doom-theme 'doom-moonlight)
;; (setq doom-theme 'doom-palenight)

(setq doom-theme 'catppuccin)
(setq catppuccin-flavor 'frappe) ;; or 'latte, 'macchiato, or 'mocha
;(catppuccin-reload)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Dropbox/OrgNotes/")

(defun org-header-style-oceanic-next ()
  (interactive)
  (dolist
      (face
       '((org-level-1 1.4 "#8caaee" extra-bold)
         (org-level-2 1.3 "#e78284" extra-bold)
         (org-level-3 1.2 "#ca9ee6" extra-bold)
         (org-level-4 1.1 "#81c8be" extra-bold)
         (org-level-5 1.0 "#eebebe" extra-bold)
         (org-level-6 1.0 "#a6d189" extra-bold)
         (org-level-7 1.0 "#99d1db" extra-bold)
         (org-level-8 1.0 "#f2d5cf" extra-bold)))
    (set-face-attribute (nth 0 face) nil
                        :font doom-variable-pitch-font
                        :weight (nth 3 face)
                        :height (nth 1 face)
                        :foreground (nth 2 face))))
(after! org
  ;(org-header-style-oceanic-next)
  (setq rainbown-mode 't))

;; org-download :)
;; (after! org
;;   (setq org-download-method 'directory)
;;   (setq org-download-image-dir "Images")
;;   (setq org-download-heading-lvl 0)
;;   (setq org-download-timestamp "%Y%m%d-%H%M%S_"))
;; (add-hook 'dired-mode-hook 'org-download-enable)
(after! org-download
      (setq org-download-method 'directory)
      (setq org-download-image-dir (concat (file-name-sans-extension (buffer-file-name)) "-img"))
      ;; (setq org-download-image-org-width 600)
      (setq org-download-link-format "[[file:%s]]\n"
        org-download-abbreviate-filename-function #'file-relative-name)
      (setq org-download-link-format-function #'org-download-link-format-function-default))


;; If you use `org-jounal' and don't want your journals in the default location below
;; change `org-jounal-directoy'. It must be set before org loads
(setq org-journal-dir "~/Dropbox/OrgNotes/Journal/")
(setq org-journal-file-format "%Y-%m-%d.org")
(setq org-journal-date-format "%A, %d %B %Y")
(setq org-journal-file-type 'monthly)

;; Added a way to open and navigate the journal
(map! :leader
      (:prefix-map ("n" . "notes")
       (:prefix ("j" . "journal")
        :desc "Open current journal" "o" #'org-journal-open-current-journal-file
        :desc "Next journal entry" "h" #'org-journal-next-entry
        :desc "Previous journal entry" "l" #'org-journal-previous-entry)))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)
(setq display-line-numbers-type 'relative)

; Set the priorities for each level of task
(setq org-fancy-priorities-list '("⚡" "⬆" "⬇" "☕"))

(add-hook! 'rainbow-mode-hook
  (hl-line-mode (if rainbow-mode -1 +1)))

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Color icons on doom treemacs
(setq doom-themes-treemacs-theme "doom-colors")

;; Default C indentation style
(setq c-default-style "user")

;; C/C++ configuration
;; (setq lsp-clients-clangd-args '("-j=3"
;; 				"--background-index"
;; 				"--clang-tidy"
;; 				"--completion-style=detailed"
;; 				"--header-insertion=never"
;; 				"--header-insertion-decorators=0"))
;; (after! lsp-clangd (set-lsp-priority! 'clangd 2))

(after! ccls
  (setq ccls-initialization-options '(:index (:comments 2) :completion (:detailedLabel t)))
  (set-lsp-priority! 'ccls 2)) ; optional as ccls is the default in Doom

;; Enable ccls for all c++ files, and platformio-mode only
;; when needed (platformio.ini present in project root).
(require 'platformio-mode)
(add-hook 'c++-mode-hook (lambda ()
                           (lsp-deferred)
                           (platformio-conditionally-enable)))

(after! evil-snipe
  (setq evil-snipe-scope 'buffer)
  (setq evil-snipe-repeat-scope 'buffer))

;; I like my editor to be kinda transparent :D
(set-frame-parameter nil 'alpha-background 95) ; For current frame
(add-to-list 'default-frame-alist '(alpha-background . 95)) ; For all new frames henceforth

(defun my-weebery-is-always-greater ()
  (let* ((banner '("______ _____ ____ ___ ___"
                   "`  _  V  _  V  _ \\|  V  ´"
                   "| | | | | | | | | |     |"
                   "| | | | | | | | | | . . |"
                   "| |/ / \\ \\| | |/ /\\ |V| |"
                   "|   /   \\__/ \\__/  \\| | |"
                   "|  /                ' | |"
                   "| /     E M A C S     \\ |"
                   "´´                     ``"))
         (longest-line (apply #'max (mapcar #'length banner))))
    (put-text-property
     (point)
     (dolist (line banner (point))
       (insert (+doom-dashboard--center
                +doom-dashboard--width
                (concat line (make-string (max 0 (- longest-line (length line))) 32)))
               "\n"))
     'face 'doom-dashboard-banner)))

(setq +doom-dashboard-ascii-banner-fn #'my-weebery-is-always-greater)

(setq evil-goggles-pulse t)
(setq evil-goggles-duration 1.250) ;; default is 0.200
