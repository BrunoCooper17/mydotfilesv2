### ALIASES ###
# Local binaries
set -U fish_user_paths $HOME/.local/bin $fish_user_paths

# Unreal engine projects
export UEProjDir="$HOME/Unreal"

# npm (Resolve EACCES permissions errors)
set -U fish_user_paths $HOME/.npm-global/bin $fish_user_paths
set -U fish_user_paths $XDG_DATA_HOME/nvim/mason/bin $fish_user_paths
###############
