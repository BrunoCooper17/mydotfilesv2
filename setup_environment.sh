#!/usr/bin/env bash

echo ""
echo "Bruno's archlinux environment :D"
# BASEDIR=$(cd "$(dirname "$0")" && pwd)


source "install_scripts/00_Setup_Environment_Variables.sh"
"${DOTFILES_SCRIPTS}/01_Config_Pacman.sh"
"${DOTFILES_SCRIPTS}/02_Config_Makepkg.sh"
"${DOTFILES_SCRIPTS}/03_Install_Paru.sh"
"${DOTFILES_SCRIPTS}/04_Config_Packages.sh"
"${DOTFILES_SCRIPTS}/05_Create_Directories.sh"
"${DOTFILES_SCRIPTS}/06_Setup_Environment_Variables.sh"
"${DOTFILES_SCRIPTS}/07_Setup_Keymaps.sh"
"${DOTFILES_SCRIPTS}/08_Miscellaneous_Scripts.sh"
"${DOTFILES_SCRIPTS}/09_Final_Steps.sh"


echo "We're done :D. In theory, only the GPU Driver is missing (Maybe :D)"
echo "If you this machine have a Nvidia GPU, run nvidia.sh (desktop)"
echo "or nvidia_laptop.sh (for my old Asus Laptop)."
echo "Intel or AMD GPUs read Arch's Docs online please."
echo ""
echo "If need any grub theme, here's my suggestion:"
echo "  https://github.com/vinceliuice/grub2-themes"
