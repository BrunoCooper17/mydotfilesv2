#!/usr/bin/env bash

echo ""
echo "Setting some environment variables first"
echo "and installing the bare minimun to make packages"
sudo pacman -Syu --needed base-devel pacman-contrib

# XDG PATHS
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"

# DOTFILES LOCATION
export DOTFILES="${HOME}/.dotfiles"
export DOTFILES_SCRIPTS="${DOTFILES}/install_scripts"

# ZSH CONFIG DIR & HISTORY
export ZDOTDIR="${XDG_CONFIG_HOME}/zsh"
export HISTFILE="${ZDOTDIR}/zsh_history"
