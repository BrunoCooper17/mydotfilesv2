#!/usr/bin/env bash

echo ""
echo "Editing makepkg.conf. I will:"
echo "   1) Change -march=x86-64 to -march=native"
echo "   2) Change RUSTFLAGS to use target-cpu=native"
echo "   3) Change MAKEFLAGS to use all your CPU when making packages"
sudo sed -i "s/\-march\=.*-mtune=generic/\-march\=native/" /etc/makepkg.conf
sudo sed -i "/\#RUSTFLAGS/ { s/\#//; s/.$/\ target-cpu=native\"/ }" /etc/makepkg.conf
CPU_THREADS=$(($(grep -c processor /proc/cpuinfo) + 1))
sudo sed -i "/MAKEFLAGS\=/ { s/\#//; s/.$//; s/.$/${CPU_THREADS}\"/ }" /etc/makepkg.conf
