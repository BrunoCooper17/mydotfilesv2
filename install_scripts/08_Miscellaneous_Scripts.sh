#!/usr/bin/env bash

: "${USE_WAYLAND:=0}"
: "${CATPPUCCIN_THEME:='Frappe'}"
: "${CATPPUCCIN_COLOR_VARIANT:='Blue'}"

# .gitconfig probably was created by git
# Remove it before setting up ours
rm "${HOME}/.gitconfig"

# Link all our dotfiles :d
"${DOTFILES_SCRIPTS}/stow_configs.sh"

# Apps and other packages that are from other places other than Arch repositories
# (or that requiere special configuration)
"${DOTFILES_SCRIPTS}/doom_emacs.sh"
# "${DOTFILES_SCRIPTS}/neovim_astrovim.sh"

# WindowManager (and partial desktop environment)
# TODO: Add a Wayland compositor install script
if [ $USE_WAYLAND -eq 0 ]; then
    # "${DOTFILES_SCRIPTS}/polybar.sh"
    "${DOTFILES_SCRIPTS}/rofi.sh"
    "${DOTFILES_SCRIPTS}/dwm.sh"
    "${DOTFILES_SCRIPTS}/dwmblocks.sh"
fi

"${DOTFILES_SCRIPTS}/sddm.sh"
"${DOTFILES_SCRIPTS}/latex_eisvogel.sh"

THEME_SCRIPTS="${DOTFILES_SCRIPTS}/themes"
"${THEME_SCRIPTS}/catppuccin-gtk-themes.sh" $CATPPUCCIN_THEME $CATPPUCCIN_COLOR_VARIANT dark
"${THEME_SCRIPTS}/catppuccin-icon-theme.sh" $CATPPUCCIN_THEME $CATPPUCCIN_COLOR_VARIANT
"${THEME_SCRIPTS}/catppuccin-kvantum-themes.sh" $CATPPUCCIN_THEME $CATPPUCCIN_COLOR_VARIANT
"${THEME_SCRIPTS}/catppuccin-papirus-folders.sh" $CATPPUCCIN_THEME $CATPPUCCIN_COLOR_VARIANT
