#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Installing Paru"
mkdir "${XDG_CACHE_HOME}"
cd "$XDG_CACHE_HOME" || return
git clone --depth 1 https://aur.archlinux.org/paru-bin.git
cd "paru-bin" || return
makepkg -sic
sudo sed -i "s/\#CleanAfter/CleanAfter/" /etc/paru.conf
sudo sed -i "s/\#\[bin\]/\[bin\]/; s/\#FileManager\ \=\ vifm/FileManager\ \=\ lf/" /etc/paru.conf
cd "$BASEDIR" || return
