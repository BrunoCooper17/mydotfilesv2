#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

Flavor="Frappe"
Colour="Blue"

if [ $# -gt 0 ]; then
    Flavor="${1}"
fi

if [ $# -gt 1 ]; then
    Colour="${2}"
fi

echo "Installing Kvantum Catppuccin Theme!"
echo "[Flavor: ${Flavor}] [Colour: ${Colour}]"
echo ""

PackageName="kvantum"
echo "Verify ${PackageName} is already installed"

PackageExist=$(paru -Qe "${PackageName}" 2>/dev/null | wc -l)
if [ ! $PackageExist ]; then
    paru -S ${PackageName}
fi

cd "${XDG_CACHE_HOME}" || return
if [ -d "${XDG_CACHE_HOME}/Kvantum" ]; then
    rm -rf "Kvantum"
fi

echo ""
echo "Cloning Catppuccin's Kvantum repository"
git clone --depth 1 https://github.com/catppuccin/Kvantum.git

ThemeLocation="${XDG_CONFIG_HOME}/Kvantum"
if [ -d "${ThemeLocation}" ]; then
    rm -rf "${ThemeLocation}"
    mkdir -p "${ThemeLocation}"
fi

echo ""
echo "Setting up theme Catppuccin-${Flavor}-${Colour}"
cp -r "${XDG_CACHE_HOME}/Kvantum/src/Catppuccin-${Flavor}-${Colour}" "${ThemeLocation}"
echo "[General]" >"${ThemeLocation}/kvantum.kvconfig"
echo "theme=Catppuccin-${Flavor}-${Colour}" >>"${ThemeLocation}/kvantum.kvconfig"

cd "${BASEDIR}" || return

echo "Kvantum Catppuccin Theme Installed Successfully :D"
