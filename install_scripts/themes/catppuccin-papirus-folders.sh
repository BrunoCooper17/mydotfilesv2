#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

Flavor="frappe"
Colour="blue"

if [ $# -gt 0 ]; then
    Flavor=$(echo "$1" | tr '[:upper:]' '[:lower:]')
fi

if [ $# -gt 1 ]; then
    Colour=$(echo "$2" | tr '[:upper:]' '[:lower:]')
fi

echo "Installing Papirus Folder Catppuccin Theme!"
echo "[Flavor: ${Flavor}] [Colour: ${Colour}]"
echo ""

PackageName="papirus-icon-theme"
echo "Verify ${PackageName} is already installed"
PackageExist=$(paru -Qe "${PackageName}" 2>/dev/null | wc -l)
if [ ! $PackageExist ]; then
    paru -S ${PackageName}
fi

cd "${XDG_CACHE_HOME}" || return
if [ -d "${XDG_CACHE_HOME}/papirus-folders" ]; then
    rm -rf "papirus-folders"
fi

echo ""
echo "Cloning Catppuccin's Papirus Folder repository"
git clone --depth 1 https://github.com/catppuccin/papirus-folders.git
sudo cp -r papirus-folders/src/* /usr/share/icons/Papirus
papirus-folders/papirus-folders -C "cat-${Flavor}-${Colour}" --theme Papirus-Dark

cd "${BASEDIR}" || return

echo "Papirus Folder Catppuccin Theme Installed Successfully :D"
