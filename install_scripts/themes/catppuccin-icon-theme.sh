#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

Flavor="Frappe"
Colour="Blue"

if [ $# -gt 0 ]; then
    Flavor="${1}"
fi

if [ $# -gt 1 ]; then
    Colour="${2}"
fi

echo "Installing Catppuccin Mouse Icons!"
echo "[Flavor: ${Flavor}] [Colour: ${Colour}]"
echo ""

PackageName="unzip"
echo "Verify ${PackageName} is already installed"
PackageExist=$(paru -Qe "${PackageName}" 2>/dev/null | wc -l)
if [ ! $PackageExist ]; then
    paru -S ${PackageName}
fi

cd "${XDG_CACHE_HOME}" || return
if [ -d "${XDG_CACHE_HOME}/cursors" ]; then
    rm -rf "cursors"
fi

echo ""
echo "Cloning Catppuccin's Mouse Icons repository"
git clone --depth 1 https://github.com/catppuccin/cursors.git

IconsLocation="${HOME}/.icons"

CatppuccinIconsExist=$(ls "${IconsLocation}/Catppuccin-*" >/dev/null 2>&1 | wc -l)
if [ $CatppuccinIconsExist ]; then
    find "${IconsLocation}" -type d -name "Catppuccin-*" -exec rm -rv {} +
fi

echo ""
Icons="Catppuccin-${Flavor}-${Colour}-Cursors"
echo "Setting up ${Icons}"
unzip "${XDG_CACHE_HOME}/cursors/cursors/${Icons}.zip" -d "${IconsLocation}"

cd "${BASEDIR}" || return

echo "Catppuccin Mouse Icons Installed Successfully :D"
