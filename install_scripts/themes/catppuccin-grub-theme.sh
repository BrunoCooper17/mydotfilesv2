#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

Flavor="frappe"

if [ $# -gt 0 ]; then
    Flavor=$(echo "$1" | tr '[:upper:]' '[:lower:]')
fi

echo "Installing Grub Catppuccin Theme!"
echo "[Flavor: $Flavor]"
echo ""

cd "${XDG_CACHE_HOME}" || cd "${BASEDIR}" && exit
git clone https://github.com/catppuccin/grub.git || cd "${BASEDIR}" && exit

CatppuccinTheme="catppuccin-${Flavor}-grub-theme"
GrubThemesDir="/usr/share/grub/themes"
Theme="${GrubThemesDir}/${CatppuccinTheme}"

if [ -d $GrubThemesDir ]; then
    sudo rm -rf "${Theme}"
fi

sudo mkdir "${Theme}"
sudo cp -r grub/src/* "${GrubThemesDir}"

# Setup Catppuccin Grub theme
sudo sed -i 's/^\#GRUB_THEME/GRUB_THEME/' /etc/default/grub
sudo sed -i "s/GRUB_THEME=*/GRUB_THEME=${GrubThemesDir}/theme.txt" /etc/default/grub

cd "${BASEDIR}" || return

echo "Grub Catppuccin Theme Installed Successfully :D"
