#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

Flavor="Frappe"
Colour="Blue"
Type="dark"

if [ $# -gt 0 ]; then
    Flavor="${1}"
fi

if [ $# -gt 1 ]; then
    Colour="${2}"
fi

if [ $# -gt 2 ]; then
    Type="${3}"
fi

echo "Installing Gtk+ Catppuccin Theme!"
echo "[Flavor: $Flavor] [Colour: $Colour] [Type: $Type]"
echo ""

PackageName="unzip"
echo "Verify ${PackageName} is already installed"
echo ""
if [ ! $(paru -Qe "${PackageName}" 2>/dev/null | wc -l) ]; then
    paru -S ${PackageName}
fi

cd "${XDG_CACHE_HOME}" || return
if [ -d "${XDG_CACHE_HOME}/gtk" ]; then
    rm -rf "gtk"
fi

echo ""
echo "Cloning Catppuccin's Gtk+ repository"
echo ""

ThemeLocation="${HOME}/.themes"

if [ $(ls "${ThemeLocation}/Catppuccin-*" >/dev/null 2>&1 | wc -l) ]; then
    find "${ThemeLocation}" -type d -name "Catppuccin-*" -exec rm -rv {} +
fi

echo ""
ThemeName="Catppuccin-${Flavor}-Standard-${Colour}-${Type}"
echo "Setting up theme ${ThemeName}"
echo ""
ThemeDownloadDir="${XDG_CACHE_HOME}/gtk/Releases/"
wget -P "${ThemeDownloadDir}" "https://github.com/catppuccin/gtk/releases/latest/${ThemeName}.zip"
unzip "${ThemeDownloadDir}${ThemeName}" -d "${ThemeLocation}"

cd "${BASEDIR}" || return

echo "Gtk+ Catppuccin Theme Installed Successfully :D"
