#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Installing nvidia drivers for my old Asus Laptop"

sudo paru -S nvidia nvidia-utils
sudo cp "${DOTFILES}/install_scripts/nvidia/10-nvidia-drm-outputclass.conf" /etc/X11/xorg.conf.d
sudo cp "${DOTFILES}/install_scripts/nvidia/nvidia.hook" /etc/pacman.d/hooks

echo "xrandr --setprovideroutputsource modesetting NVIDIA-0" | sudo tee -a /usr/share/sddm/scripts/Xsetup
echo "xrandr --auto" | sudo tee -a /usr/share/sddm/scripts/Xsetup

cd "${BASEDIR}" || return

echo "Nvidia drivers Successfully installed"
