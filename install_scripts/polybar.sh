#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Installing polybar"
# cd "${XDG_CACHE_HOME}" || return
# git clone --depth 1 https://aur.archlinux.org/polybar.git
# cd polybar || return
# cp "${DOTFILES}/polybar/dot-config/polybar/nwin.patch" .
# sed -i "s/source=(\${/source=(nwin.patch\ \${/" PKGBUILD
# sed -ie "/mkdir -p \"\${_dir}\/build\"/a\ \ cd \"\${srcdir}\/\${_dir}\"\n\ \ patch --forward --strip=1 --input=\${srcdir}\/nwin.patch" PKGBUILD
# updpkgsums
# makepkg -sic
paru -S polybar

sudo mkdir /etc/pacman.d/hooks
sudo cp "${DOTFILES}/polybar/dot-config/polybar/update-polybar-status-bar.hook" /etc/pacman.d/hooks
sudo cp "${DOTFILES}/polybar/dot-config/polybar/95-usb.rules" /etc/udev/rules.d

echo "*/3 * * * * /usr/bin/polybar-msg hook updates-pacman-aur 1" | crontab -

USER=$(whoami)
sudo sed -i "s/\/home\/user/\/home\/${USER}/" /etc/udev/rules.d/95-usb.rules

cd "${BASEDIR}" || return
echo "Polybar installed successfully"
