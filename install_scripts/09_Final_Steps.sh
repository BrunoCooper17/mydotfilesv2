#!/usr/bin/env bash

## Icon permissions for sddm
ln -si "${HOME}/.face" "${HOME}/.face.icon"
setfacl -m u:sddm:x ~/
setfacl -m u:sddm:r ~/.face.icon
