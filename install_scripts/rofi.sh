#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Install Rofi"
cd "${XDG_CACHE_HOME}" || return
git clone --depth 1 https://github.com/adi1090x/rofi.git
cd "rofi" || return
chmod +x setup.sh
./setup.sh
cd "${BASEDIR}" || return

echo "Rofi installed successfully"
