#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Installing nvidia drivers"

sudo paru -S nvidia nvidia-utils
sudo cp "${DOTFILES}/install_scripts/nvidia/nvidia.hook" /etc/pacman.d/hooks
sudo cp "${DOTFILES}/install_scripts/nvidia/10-monitor.conf" /etc/X11/xorg.conf.d/
cd "$BASEDIR" || return

echo "Nvidia drivers Successfully installed"
