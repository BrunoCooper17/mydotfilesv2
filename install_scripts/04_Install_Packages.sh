#!/usr/bin/env bash

: "${USE_WAYLAND:=0}"
: "${INSTALL_GUITARIX:=0}"
: "${INSTALL_DOCKER:=0}"
: "${INSTALL_PLYMOUTH:=0}"

echo ""
echo "Installing everything we need :)"

BROWSERS="brave-bin vivaldi profile-sync-daemon-brave"
BLUETOOTH="bluez bluez-utils bluez-cups bluez-tools blueman"
IM_APPS="discord caprine whatsapp-nativefier slack-desktop"
INTERNET="${BROWSERS} ${BLUETOOTH} ${IM_APPS} network-manager-applet transmission-gtk thunderbird hugo dropbox wget"

GUITARIX=""
if [ $INSTALL_GUITARIX -eq 1 ]; then
    GUITARIX="guitarix gxplugins.lv2 carla fluidsynth realtime-privileges lilv lv2"
fi

AUDIO="pipewire gst-plugin-pipewire pipewire-alsa pipewire-jack pipewire-pulse pipewire-v4l2 wireplumber pamixer pasystray pavucontrol easyeffects"
MPD="mpd cantata"
MUSIC="${MPD} mpv audacious spotify stremio yt-dlp"
MULTIMEDIA="${AUDIO} ${GUITARIX} ${MUSIC} viewnior krita gmic blender"

DOCKER=""
if [ $INSTALL_DOCKER -eq 1 ]; then
    DOCKER="docker docker-buildx"
fi

# XMONAD_HASKEL="hlint-bin"
LATEX="texlive-most pandoc-bin"
LANG_UTILS="hunspell-en_us hunspell-es_mx languagetool"
CPP_COMPILERS="clang llvm llvm-libs lld"
COMPILERS="${CPP_COMPILERS} rust python python-pip"
DOOM_EMACS_DEPS="ripgrep fd shellcheck-bin tk"
EDITORS="emacs-nativecomp neovim visual-studio-code-bin"
CODE="${LATEX} ${LANG_UTILS} ${COMPILERS} ${EDITORS} ${DOOM_EMACS_DEPS} ${DOCKER} kitty starship gittyup"

if [ $USE_WAYLAND -eq 1 ]; then
    DESKTOP_WAYLAND_CORE="sww gtklock wofi xwalyand"
    DESKTOP_WAYLAND_UTILS="wlr-randr wl-clipboard"
    DESKTOP_APPS="${DESKTOP_WAYLAND_CORE} ${DESKTOP_WAYLAND_UTILS}"
else
    DESKTOP_X11_SERVER="xorg-server xf86-input-libinput libinput picom-animations-git"
    DESKTOP_X11_CORE="nitrogen betterlockscreen rofi"
    DESKTOP_X11_UTILS="arandr sxhkd xclip xsel"
    DESKTOP_APPS="${DESKTOP_X11_SERVER} ${DESKTOP_X11_CORE} ${DESKTOP_X11_UTILS}"
fi

DESKTOP_CORE="sddm thunar lxsession systemsettings dunst cups cronie sane-airscan"
DESKTOP_UTILS="gnome-keyring zenity udiskie gdu light xdg-user-dirs xdg-utils rate-mirrors"
DESKTOP_THEMES="lxappearance-gtk3 gtk-engine-murrine vivid kvantum qt5ct papirus-icon-theme gnome-themes-extra"
DESKTOP_GAMES="steam openrct2 brutal-doom"
DESKTOP_MISC_APPS="texturepacker spectacle zathura sane skanpage"
DESKTOP_ENV="${DESKTOP_APPS} ${DESKTOP_CORE} ${DESKTOP_UTILS} ${DESKTOP_THEMES} ${DESKTOP_GAMES} ${DESKTOP_MISC_APPS}"

LF_UTILS="poppler gnome-epub-thumbnailer perl-file-mimeinfo"
LF_RANGER="${LF_UTILS} lf atool elinks ffmpegthumbnailer jq highlight mediainfo odt2txt transmission-cli"
COMMAND_LINE="fish fisher grml-zsh-config exa bat sysstat stow-dotfiles-git"

NOTO_FONTS="noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra"
NERD_FONTS="nerd-fonts ttf-jetbrains-mono-nerd"
AWESOME_FONTS="ttf-font-awesome"
TTF_FONTS="ttf-dejavu ttf-liberation ttf-roboto"
MISC_FONTS="siji-git"
FONTS="${NOTO_FONTS} ${NERD_FONTS} ${AWESOME_FONTS} ${TTF_FONTS} ${MISC_FONTS}"

PLYMOUTH=""
if [ $INSTALL_PLYMOUTH -eq 1 ]; then
    PLYMOUTH="plymouth uvesafb-dkms"
fi

paru -S ${INTERNET} ${MULTIMEDIA} ${CODE} ${DESKTOP_ENV} ${LF_RANGER} ${COMMAND_LINE} ${FONTS} ${PLYMOUTH}
