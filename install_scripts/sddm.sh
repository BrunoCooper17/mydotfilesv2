#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Setting SDDM theme"
cd "${XDG_CACHE_HOME}" || return
git clone --depth 1 https://github.com/yeyushengfan258/Lyra-kde.git
sudo cp -r Lyra-kde/sddm/Lyra /usr/share/sddm/themes
sudo cp "${DOTFILES}/wallpapers/Wallpapers/Zootopia/ZootopiaWallpaper13.jpg" /usr/share/sddm/themes/Lyra/background.jpg
sudo mkdir /etc/sddm.conf.d
sudo cp /usr/lib/sddm/sddm.conf.d/default.conf /etc/sddm.conf.d/sddm.conf
sudo sed -i "s/Current=/Current=Lyra/; s/Numlock=none/Numlock=on/; s/EnableHiDPI=false/EnableHiDPI=true/g; s/-nolisten tcp/-nolisten tcp -dpi 118/g" /etc/sddm.conf.d/sddm.conf
cd "${BASEDIR}" || return

echo "SDDM configured"
