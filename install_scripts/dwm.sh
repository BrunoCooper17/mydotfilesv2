#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Install Window Manager (Dwm)"
cd "${XDG_CACHE_HOME}" || return
git clone --depth 1 https://gitlab.com/BrunoCooper17/dwm-rebirth.git
cd dwm-rebirth || return
./compile-dwm
cd "${BASEDIR}" || return

echo "Dwm successfully installed :D"
