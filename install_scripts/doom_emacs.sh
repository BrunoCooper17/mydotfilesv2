#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Installing Doom Emacs!"
cd "${HOME}" || cd $BASEDIR && return

if [ -d "${HOME}/.emacs.d" ]; then
    rm -rf .emacs.d
fi

DOOM_LOCATION="${XDG_CONFIG_HOME}/emacs"
if [ -d "${DOOM_LOCATION}" ]; then
    rm -rf "${DOOM_LOCATION}"
fi

git clone --depth 1 https://github.com/doomemacs/doomemacs "${DOOM_LOCATION}"
"${DOOM_LOCATION}/bin/doom" install

cd "${BASEDIR}" || return

echo "Doom Emacs Installed Successfully :D"
