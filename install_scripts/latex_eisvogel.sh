#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Install Eisvogel latex template"
cd "${XDG_CACHE_HOME}" || cd "${BASEDIR}" && return

git clone --depth 1 https://github.com/Wandmalfarbe/pandoc-latex-template.git
mkdir -p "${XDG_DATA_HOME}/pandoc/templates"
cp pandoc-latex-template/eisvogel.tex "${XDG_DATA_HOME}/pandoc/templates"

cd "${BASEDIR}" || return

echo "Latex's Eisvogel template installed"
