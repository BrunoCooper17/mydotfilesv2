#!/usr/bin/env bash

echo ""
echo "Editing pacman-conf. I will:"
echo "   1) Enable pacman hooks"
echo "   2) Enable Colors"
echo "   3) Enable Parallel Downloads"
echo "   4) Add ILoveCandy <3"
echo "   5) Enable Multilib repository"
echo "   6) Add Herecura repository"
sudo sed -i "s/^\#HookDir/HookDir" /etc/pacman.conf
sudo sed -i "s/^\#Color/Color/; s/^\#ParallelDownloads/ParallelDownloads/" /etc/pacman.conf
sudo sed -i "/^ParallelDownloads*/a ILoveCandy" /etc/pacman.conf
sudo sed -ie "/^\#\[multilib\]/ { s/^\#//; n; s/^\#Include/Include/ }" /etc/pacman.conf
echo "[herecura]" | sudo tee -a /etc/pacman.conf
echo "# packages built against stable" | sudo tee -a /etc/pacman.conf
echo "Server = https://repo.herecura.eu/herecura/x86_64" | sudo tee -a /etc/pacman.conf
sudo pacman -Syy
