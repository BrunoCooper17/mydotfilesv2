#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)

echo "Install Status Bar (Dwmblocks)"
cd "${XDG_CACHE_HOME}" || return
git clone --depth 1 https://gitlab.com/BrunoCooper17/mydwmblocks.git
cd mydwmblocks || return
./compile-dwmblocks
cd "${BASEDIR}" || return

sudo mkdir /etc/pacman.d/hooks
sudo cp "${DOTFILES}/install-scripts/dwmblocks/update-dwm-status-bar.hook" /etc/pacman.d/hooks
sudo cp "${DOTFILES}/polybar/dot-config/polybar/95-usb.rules" /etc/udev/rules.d

USER=$(whoami)
sudo sed -i "s/\/home\/user/\/home\/${USER}/" /etc/udev/rules.d/95-usb.rules

echo "Dwmblocks successfully installed :D"
