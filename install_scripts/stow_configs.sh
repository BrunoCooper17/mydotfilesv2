#!/usr/bin/env bash
BASEDIR=$(cd "$(dirname "$0")" && pwd)
RESTOW=false
DELETE=false

while [ "$1" != "" ]; do
    case $1 in
        --restow)
            RESTOW=true
            ;;
        --delete)
            DELETE=true
            ;;
    esac
    shift # remove the current value for `$1` and use the next
done

echo ""
echo "Stow magic for my config files"
cd "${DOTFILES}" || $BASEDIR && return

ArrayStow=($(exa --ignore-glob="install_scripts|*.sh|*.org|.*"))
for config in "${ArrayStow[@]}"; do
    if [[ $DELETE == true ]]; then
        stow --verbose --target="${HOME}" --dotfiles --delete "${config}"
    elif [[ $RESTOW == true ]]; then
        stow --verbose --target="${HOME}" --dotfiles --restow "${config}"
    else
        stow --verbose --target="${HOME}" --dotfiles --stow "${config}"
    fi
done

cd "${BASEDIR}" || return

echo "Stow configurations done"
