#!/usr/bin/env bash

# Create User directories
xdg-user-dirs-update

# For mpd to work
mkdir -p "${HOME}/.mpd/playlists"
# For ssh-agent
mkdir "${HOME}/.ssh"
# npm to avoid EACCS permissions errors
mkdir "${HOME}/.npm-global"
