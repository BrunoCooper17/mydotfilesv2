#!/usr/bin/env bash

echo ""
echo "Setup systemd services"

# systemctl --user enable emacs
systemctl --user enable psd
systemctl --user enable mpd.socket
systemctl --user enable pipewire-pulse.service
sudo systemctl enable bluetooth
sudo systemctl enable sddm
sudo systemctl enable cronie
sudo systemctl enable cups.socket
