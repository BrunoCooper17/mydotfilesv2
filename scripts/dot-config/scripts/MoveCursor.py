# THIS SCRIPT IS USED TO MOVE THE CURSOR TO THE NEW WINDOW IN FOCUS
# (CURRENTLY IS CALLED BY ACTIONS IN XMONAD, BUT I THINK IT CAN BE USED BY ANY OTHER WM/DE)

import io
import subprocess
import time
import sys

windowX = 0
windowY = 0
sizeX = 0
sizeY = 0

screenData = sys.argv[1] + ":"
xdotoolSuccess = False

# WAIT A LITTLE MORE BEFORE GATHERING INFORMATION AND MOVE THE CURSOR
time.sleep(0.01)

cmdOut = subprocess.Popen(['xdotool'
                         , 'getactivewindow'
                         , 'getwindowgeometry'
                         , '--shell']
                         , stdout = subprocess.PIPE)

for line in io.TextIOWrapper(cmdOut.stdout, encoding = 'UTF-8'):
    if 'X=' in line:
        windowX = int( line[2:] )
        xdotoolSuccess = True
    elif 'Y=' in line:
        windowY = int( line[2:] )
    elif 'WIDTH=' in line:
        sizeX = int( line[6:] )
    elif 'HEIGHT=' in line:
        sizeY = int( line[7:] )

if xdotoolSuccess == False:
    monitors = subprocess.Popen(['xrandr'
                               , '--listactivemonitors']
                               , stdout = subprocess.PIPE)
    for line in io.TextIOWrapper(monitors.stdout, encoding = 'UTF-8'):
        if screenData in line:
            # Resolution in format ['X/sizemm x Y/sizemm', +offsetX, +offsetY] (1920/477x1080/268+1080+380)
            resolutionRaw = line.split(' ')[3].split('+')
            resolutionRawXY = resolutionRaw[0].split('x')

            sizeX = int( resolutionRawXY[0].split('/')[0] )
            sizeY = int( resolutionRawXY[1].split('/')[0] )
            windowX = int( resolutionRaw[1] )
            windowY = int( resolutionRaw[2] )

# MOVE THE CURSOR TO THE CENTER OF THE WINDOW FOCUSED
subprocess.Popen(['xdotool'
                , 'mousemove'
                , str(windowX + sizeX * 0.5)
                , str(windowY + sizeY * 0.5)])