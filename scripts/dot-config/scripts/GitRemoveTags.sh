#!/bin/bash

insideGitRepo="$(git rev-parse --is-inside-work-tree 2>/dev/null)"

if [ "$insideGitRepo" ]
then
    if [ $# -gt 0 ]
    then
        for Tag in $@
        do
            echo "Removing tag ->" $Tag
            git push --delete origin $Tag
            git tag --delete $Tag
            echo ""
        done
        echo $# "tags removed :)"
    else
        echo "No tags provided"
    fi
else
    echo "Not in a Git directory"
fi