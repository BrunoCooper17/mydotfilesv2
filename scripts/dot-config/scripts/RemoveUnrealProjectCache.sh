#!/bin/bash

insideGitRepo="$(ls *.uproject 2>/dev/null)"

RemoveBinaries=false
RemoveIntermediate=false
ShowHelp=false

if [ $# -lt 1 ]
then
    ShowHelp=true
fi

while getopts bih option
do
    case "${option}"
    in
        b) RemoveBinaries=true;;
        i) RemoveIntermediate=true;;
        h) ShowHelp=true;;
    esac
done

if "$ShowHelp" == "true"
then
    me=`basename "$0"`
    echo "Usage:"
    echo "    " $me "[options]"
    echo ""
    echo "Options:"
    echo "     -i Remove Intermediate Directories"
    echo "     -b Remove Binaries Directories"
    echo "     -h Show this help"
    exit
fi

if [ "$insideGitRepo" ]
then
    if "$RemoveBinaries" == "true"
    then
        echo "Removing Binaries Directories"
        for i in $(find . -name Binaries)
        do
            echo "Removing" $i
            rm -r $i
        done
        echo ""
    fi

    if "$RemoveIntermediate" == "true"
    then
        echo "Removing Intermediate Directories"
        for i in $(find . -name Intermediate)
        do
            echo "Removing" $i
            rm -r $i
        done
    fi
else
    echo "Not in a Unreal Project directory"
fi