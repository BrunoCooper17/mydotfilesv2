#!/bin/bash

echo "Unlocking *.archive files"
for i in $(find . -name "*.archive")
do
    chmod 0774 $i
done

echo "Unlocking *.locres files"
for i in $(find . -name "*.locres")
do
    chmod 0774 $i
done

echo "Unlocking *.manifest files"
for i in $(find . -name "*.manifest")
do
    chmod 0774 $i
done