#!/bin/sh
# USAGE ExportOrgToPDF.sh FILENAME [SETTINGS_FILENAME]
FILE=$1
SETTINGS=$2

# Check that file exist
if [ ! -f "$FILE.org" ]
then
   echo "$FILE.org does not exist"
   exit 1
fi

if [ -z "$SETTINGS" ] || [ ! -f "$SETTINGS.yaml" ]
then
    echo "$SETTINGS file does not exist using default_Eisvogel_style.yaml file on $XDG_CONFIG_HOME/scripts"
    SETTINGS="${XDG_CONFIG_HOME}/scripts/default_Eisvogel_style"
fi

echo "EXECUTING: pandoc $FILE.org -o $FILE.pdf --template eisvogel --listings --metadata-file $SETTINGS.yaml"
pandoc $FILE.org -o $FILE.pdf --template eisvogel --listings --metadata-file $SETTINGS.yaml
