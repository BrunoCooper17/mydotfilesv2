#!/bin/bash

echo "Unlocking *.umap files"
for i in $(find . -name "*.umap")
do
    chmod 0774 $i
done

echo "Unlocking *.uasset files"
for i in $(find . -name "*.uasset")
do
    chmod 0774 $i
done