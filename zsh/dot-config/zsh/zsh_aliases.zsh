### ALIASES ###
# USE NeoVim (LunarVim)
alias vim="nvim"
alias vi="nvim"

### CAT... I mean BAT :D ###
alias cat="bat"

# DOOM EMACS!
alias doomsync="$HOME/.config/emacs/bin/doom sync"
alias doomdoctor="$HOME/.config/emacs/bin/doom doctor"
alias doomupgrade="$HOME/.config/emacs/bin/doom upgrade"
alias doompurge="$HOME/.config/emacs/bin/doom purge"

# JUST TYPE UE4Editor or UE5Editor (Unreal Engine 4/5 Editor binary)
alias UE4Editor="/opt/UnrealEngine/Engine/Binaries/Linux/UE4Editor"
alias UE5Editor="/opt/UnrealEngine/Engine/Binaries/Linux/UE5Editor"

# GET FASTEST MIRRORS
alias mirror="rate-mirrors arch | sudo tee /etc/pacman.d/mirrorlist"

# CHANGING "LS" TO "EXA"
alias ls='exa --color=always --icons --group-directories-first'       # all files and directories
alias lsa='exa -a --color=always --icons --group-directories-first'   # my preferred listing
alias lsl='exa -l --color=always --icons --group-directories-first'   # long format
alias lsla='exa -al --color=always --icons --group-directories-first' # long format
alias lst='exa -T --color=always --icons --group-directories-first'   # tree listing
alias lsta='exa -aT --color=always --icons --group-directories-first' # tree listing
alias l.='exa -a --color=always --icons | egrep "^\."'

# COLORS MAKES EVERYTHING BETTER
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# CP/MV/RM ASK FOR CONFIRMATION BEFORE OVERWRITTING/DELETING ANYTHING
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'
###############
