### Exports ###
# Local applications
export PATH=$HOME/.local/bin:$PATH

# Unreal engine projects
export UEProjDir="$HOME/Unreal"

# npm (Resolve EACCES permissions errors)
export PATH=$HOME/.npm-global/bin:$PATH
export PATH=$XDG_DATA_HOME/nvim/mason/bin:$PATH
###############
