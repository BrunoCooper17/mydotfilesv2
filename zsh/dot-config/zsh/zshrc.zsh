### Created by Zap installer ###
[ -f "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh" ] && source "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh"
################################

### Zap plugins ###
plug "zap-zsh/completions"
plug "zap-zsh/sudo"
plug "zap-zsh/supercharge"
plug "zsh-users/zsh-autosuggestions"
plug "zsh-users/zsh-syntax-highlighting"
###################

################### SSH-AGENT ###################
# (the plugin comes from the Oh-my-zsh plugins) #
zstyle :omz:plugins:ssh-agent agent-forwarding on
# zstyle :omz:plugins:ssh-agent identities BrunoKey
zstyle :omz:plugins:ssh-agent lazy yes
plug "$ZDOTDIR/plugins/ssh-agent/ssh-agent.plugin.zsh"
#################################################

### Local Files ###
plug "$ZDOTDIR/zsh_aliases.zsh"
# plug "$ZDOTDIR/zsh_completion.zsh"
plug "$ZDOTDIR/zsh_exports.zsh"
###################

### Starship prompt ###
prompt off
eval "$(starship init zsh)"
######################
