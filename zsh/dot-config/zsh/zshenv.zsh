# XDG PATHS
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state

# DOTFILES LOCATION
export DOTFILES=$HOME/.dotfiles

# DEFAULT EDITOR FOR QUICK MODIFICATIONS (neovim)
export EDITOR="nvim"
export VISUAL="nvim"

# SET LS_COLORS USING VIVID
export LS_COLORS="$(vivid generate catppuccin-frappe)"

# BAT THEME SOURCE!
bat cache --build &> /dev/null &

#### Z S H ####
# ZSH HISTORY
export HISTFILE=$ZDOTDIR/zsh_history    # History filepath
export HISTSIZE=5000                    # Maximum events for internal history
export SAVEHIST=5000                    # Maximum events in history file
export HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd -|cd ..)"
