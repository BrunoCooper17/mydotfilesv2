#!/bin/sh

killall polybar

sleep 3

for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    export MONITOR=$m
    if [ $m == "DP-3" ] || [ $m == "eDP-1-1" ] || [ $m == "LVDS-1" ]; then
	polybar primary -r -q &
    else
	polybar secondary -r -q &
    fi
done
