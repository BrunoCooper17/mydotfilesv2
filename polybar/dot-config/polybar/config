;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
blackNormal = #aa343d46
redNormal = #aaec5f67
greenNormal = #aa99c794
yellowNormal = #aafac863
blueNormal = #aa6699cc
magentaNormal = #aae27e8d
cyanNormal = #aa5fb3b3
whiteNormal = #aad8dee9

blackBright = #aa6f777f
redBright = #aaed7777
greenBright = #aaa9d4a9
yellowBright = #aafad17f
blueBright = #aa77add9
magentaBright = #aae298a3
cyanBright = #aa6bbfbf
whiteBright = #aaebf5f5

;background = ${xrdb:color0:#222}
background = #aa1b2b34
background-alt = #aa1b2b34
;foreground = ${xrdb:color7:#222}
foreground = #d8dee9
foreground-alt = #d8dee9
primary = ${self.blueNormal}
secondary = ${self.cyanNormal}
alert = ${self.redNormal}

;-------------------------------------------------------------------------------

[bar/generic]
width = 100%
height = 28
radius = 2.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 4
line-color = #f00

border-size = 4
border-color = #00000000

padding = 1

module-margin-left = 1
module-margin-right = 1

font-0 = "JetBrainsMono Nerd Font:style=Bold:size=10;1"
font-1 = "Font Awesome 5 Brands,Font Awesome 5 Brands Regular:style=Regular:size=10;1"
font-2 = "Font Awesome 5 Free,Font Awesome 5 Free Solid:style=Solid:size=10;1"
font-3 = "siji:pixelsize=12;1"

modules-left = xmonad-monitor ewmh xmonad-layout
modules-center = 
modules-right = system-usb-udev updates-pacman-aur pulseaudio memory cpu wlan eth eth-pc date

; tray-position = none

; Tray icon max size
tray-maxsize = 16

;override-redirect = true

; Set a DPI values used when rendering text
; This only affects scalable fonts
; Set this to 0 to let polybar calculate the dpi from the screen size.
dpi = 96
; dpi-x = 96
; dpi-y = 96

cursor-click = pointer
cursor-scroll = ns-resize

;-------------------------------------------------------------------------------

[bar/primary]
inherith = bar/generic
monitor = ${env:MONITOR:}
enable-ipc = true

;modules-left =
;modules-center =
;modules-right =

tray-position = right
tray-padding = 2
;tray-background = #0063ff
tray-background = ${colors.background}

;-------------------------------------------------------------------------------

[bar/secondary]
inherith = bar/generic
monitor = ${env:MONITOR:}

modules-left = ewmh
modules-center =
modules-right =

tray-position = none

;-------------------------------------------------------------------------------

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

;-------------------------------------------------------------------------------

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

;-------------------------------------------------------------------------------

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted =  %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

;-------------------------------------------------------------------------------

[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

;-------------------------------------------------------------------------------

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

;-------------------------------------------------------------------------------

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

;-------------------------------------------------------------------------------

[module/cpu]
type = internal/cpu
interval = 2

format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-underline = ${colors.redNormal}
label = %percentage:2%%
;label = %percentage-cores%

;-------------------------------------------------------------------------------

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-underline = ${colors.greenNormal}
;label = %percentage_used%%
;label = %gb_used%/%gb_free%
label = %gb_used%

;-------------------------------------------------------------------------------

[module/wlan]
type = internal/network
interface = wlp3s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = ${colors.magentaBright}

; All labels support the following tokens:
;   %ifname%    [wireless+wired]
;   %local_ip%  [wireless+wired]
;   %local_ip6% [wireless+wired]
;   %essid%     [wireless]
;   %signal%    [wireless]
;   %upspeed%   [wireless+wired]
;   %downspeed% [wireless+wired]
;   %linkspeed% [wired]
label-connected = %upspeed% %downspeed%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.foreground}

;-------------------------------------------------------------------------------

[module/eth]
type = internal/network
interface = enp2s0
interval = 3.0

format-connected-underline = ${colors.greenBright}
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground}
label-connected = %upspeed% %downspeed%

format-disconnected =
format-disconnected-prefix = " "
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/eth-pc]
inherit = module/eth
interface = enp0s0

;-------------------------------------------------------------------------------

[module/date]
type = internal/date
interval = 5

date = "%d/%m/%y"
date-alt = " %d/%h/%y"

time = %H:%M
time-alt = %H:%M
;time-alt = %H:%M:%S

format-prefix = 
format-prefix-foreground = ${colors.foreground}
format-underline = ${colors.blueNormal}

label = %date% %time%

;-------------------------------------------------------------------------------

[module/pulseaudio]
type = internal/pulseaudio

;format-volume = <label-volume> <bar-volume>
format-volume = <label-volume>
label-volume =  %percentage%%
label-volume-foreground = ${colors.foreground}

; Use PA_VOLUME_UI_MAX (~153%) if true, or PA_VOLUME_NORM (100%) if false
use-ui-max = false

click-right = exec pavucontrol &

; Interval for volume increase/decrease (in percent points)
interval = 2

label-muted = ﱝ muted
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground}

;-------------------------------------------------------------------------------

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC0
full-at = 98

format-charging = <animation-charging> <label-charging>
format-charging-underline = #ffb52a

format-discharging = <animation-discharging> <label-discharging>
format-discharging-underline = ${self.format-charging-underline}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = ${colors.foreground-alt}
animation-discharging-framerate = 750

;-------------------------------------------------------------------------------

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-underline = #f50a4d
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-alt}

;-------------------------------------------------------------------------------

[module/title]
type = internal/xwindow

; Available tags:
;   <label> (default)
format = <label>
format-background = #dd007f3f
format-foreground = #d000
format-padding = 4

; Available tokens:
;   %title%
; Default: %title%
label = %title%
label-maxlen = 20

; Used instead of label when there is no window title
; Available tokens:
;   None
label-empty = None
label-empty-foreground = #d000

;-------------------------------------------------------------------------------

[module/updates-pacman-aur]
type = custom/ipc

hook-0 = $XDG_CONFIG_HOME/polybar/scripts/updates-pacman-aur.sh
click-left = $XDG_CONFIG_HOME/polybar/scripts/updates-pacman-aur.sh
initial = 1

label-font = 2
format-underline = ${colors.cyanBright}
format-padding = 1


;-------------------------------------------------------------------------------

[module/system-usb-udev]
; Copy 95-usb.rules to /etc/udev/rules.d/95-usb.rules.
; Make sure that the paths in the file have been modified properly.
type = custom/script

label-font = 0

format-underline = ${colors.yellowNormal}
format-prefix = "禍 "
format-padding = 1

exec = $XDG_CONFIG_HOME/polybar/scripts/system-usb-udev.sh
tail = true

click-left = "$XDG_CONFIG_HOME/polybar/scripts/system-usb-udev.sh --mount &"
click-right = "$XDG_CONFIG_HOME/polybar/scripts/system-usb-udev.sh --unmount &"

;-------------------------------------------------------------------------------

[module/xmonad-layout]
type = custom/script

label-font = 0

format-underline = #00B377
format-prefix = "冷 "
format-padding = 1

exec = tail -F /tmp/xmonad-layout-log
exec-if = [ -p /tmp/xmonad-layout-log ]
tail = true

;-------------------------------------------------------------------------------

[module/xmonad-monitor]
type = custom/script

label-font = 0
format-prefix = " "

;exec = "tail -F /tmp/xmonad-screen-log"
exec = "$HOME/.config/polybar/scripts/display-monitor.sh"
exec-if = [ -p /tmp/xmonad-screen-log ]
tail = true

;-------------------------------------------------------------------------------

[module/ewmh]
type = internal/xworkspaces

; Only show workspaces defined on the same output as the bar
; Useful if you want to show monitor specific workspaces
; on different bars
; Default: false
pin-workspaces = false

; Create click handler used to focus desktop
; Default: true
enable-click = true

; Create scroll handlers used to cycle desktops
; Default: true
enable-scroll = true

icon-0 = WWW;
icon-1 = Code;
icon-2 = Terminal;
icon-3 = GFX;
icon-4 = Music;
icon-5 = Chat;
icon-default = ♟

; Available tags:
;   <label-monitor>
;   <label-state> - gets replaced with <label-(active|urgent|occupied|empty)>
; Default: <label-state>
format = <label-state>

; Available tokens:
;   %name%
; Default: %name%
label-monitor = %name%

; Available tokens:
;   %name% %icon% %index% %nwin%
; Default: %index%%icon% %name%
label-active-font = 0
label-active = %icon% [%nwin%]
; label-active-foreground = ${colors.whiteBright}
label-active-background = ${colors.blackNormal}
label-active-underline = ${colors.yellowNormal}
label-active-padding = 2

; Available tokens:
;   %name% %icon% %index% %nwin%
; Default: %icon% %name%
label-occupied-font = 0
label-occupied = %icon% [%nwin%]
label-occupied-foreground = ${colors.whiteBright}
label-occupied-underline = ${colors.greenBright}
label-occupied-padding = 1

; Available tokens:
;   %name% %icon% %index% %nwin%
; Default: %icon% %name%
label-urgent-font = 0
label-urgent = %icon% [%nwin%]
; label-urgent-foreground = ${colors.whiteNormal}
label-urgent-background = ${colors.redBright}
label-urgent-underline = ${colors.redNormal}
label-urgent-padding = 1

; Available tokens:
;   %name% %icon% %index% %nwin%
; Default: %icon% %name%
label-empty-font = 0
label-empty = %icon% [%nwin%]
label-empty-foreground = #888
label-empty-padding = 1

;-------------------------------------------------------------------------------

[module/powermenu]
type = custom/menu

expand-right = true
format-spacing = 1

;;label-open = !!
;;label-open-foreground = ${colors.secondary}
;;label-close =  cancel
;;label-close-foreground = ${colors.secondary}
;;label-separator = |
;;label-separator-foreground = ${colors.foreground-alt}

;;menu-0-0 = reboot
;;menu-0-0-exec = menu-open-1
;;menu-0-1 = power off
;;menu-0-1-exec = menu-open-2

;;menu-1-0 = cancel
;;menu-1-0-exec = menu-open-0
;;menu-1-1 = reboot
;;menu-1-1-exec = sudo reboot

;;menu-2-0 = power off
;;menu-2-0-exec = sudo shutdown now
;;menu-2-1 = cancel
;;menu-2-1-exec = menu-open-0

;-------------------------------------------------------------------------------

[settings]
screenchange-reload = true
;compositing-background = xor
compositing-background = source
compositing-foreground = source
compositing-border = over
;pseudo-transparency = false

;-------------------------------------------------------------------------------

[global/wm]
margin-top = 7
margin-bottom = -5
;margin-bottom = 1

; vim:ft=dosini
